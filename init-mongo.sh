#!/bin/bash
set -e

mongo <<EOF
use baanabaanaDB
db.createUser({
  user:  'root123',
  pwd: 'pwd1234',
  roles: [
   {
      role: 'userAdmin',
      db: 'baanabaanaDB'
    },
    {
      role: 'readWrite',
      db: 'baanabaanaDB'
    }
  ]
})
EOF
